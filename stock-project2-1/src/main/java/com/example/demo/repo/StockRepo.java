package com.example.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.example.demo.model.Stock;

@RepositoryRestResource(collectionResourceRel="stock", path="stock")
public interface StockRepo extends JpaRepository<Stock, Integer>{

}
