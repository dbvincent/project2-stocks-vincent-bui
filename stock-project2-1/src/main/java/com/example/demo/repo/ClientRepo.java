package com.example.demo.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.example.demo.model.Client;

@RepositoryRestResource(collectionResourceRel="client", path="client")
public interface ClientRepo extends JpaRepository<Client, Integer>{
	List<Client> findByUsername(String username);
}
