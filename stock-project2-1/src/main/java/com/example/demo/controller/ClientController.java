package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.repo.ClientRepo;
import com.example.demo.repo.StockRepo;
import com.example.demo.model.Client;

@RestController
@RequestMapping("restapi")
public class ClientController {
	@Autowired
	ClientRepo clientRepo;
	@Autowired
	StockRepo stockRepo;
	
	@RequestMapping("/hello")
	public String restClient() {
		return "Hello from client controller";
	}
	
	@GetMapping("client")
	public List<Client> displayAllClients() {
		return clientRepo.findAll();
	}
}

