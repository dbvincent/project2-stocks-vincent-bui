package com.example.demo.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.StockToDisplay;
import com.example.demo.repo.ClientRepo;
import com.example.demo.repo.StockRepo;
import com.google.gson.Gson;

@Controller
@RequestMapping("portfolio")
public class PortfolioController {
	@Autowired
	ClientRepo clientRepo;
	@Autowired
	StockRepo stockRepo;

	@GetMapping
	public ModelAndView displayPortfolio(Model model) {
		ModelAndView mv;
		mv = new ModelAndView();
		mv.setViewName("portfolio/index");

		//System.out.println("hello"+ model.getAttribute("existingclient"));
		//System.out.println(StockInfo.getStock("AAPL"));
		StockToDisplay stockToDisplay = getStock("AMZN");
		model.addAttribute("stockToDisplay", stockToDisplay);		
		mv.addObject("stockinfo", stockToDisplay);
		
		return mv;
	}
	@GetMapping("stockdetail")
	public ModelAndView displayStockDetail(Model model) {
		ModelAndView mv;
		mv = new ModelAndView();
		mv.setViewName("portfolio/stockdetail");

		StockToDisplay stockToDisplay = getStock("AMZN");
		model.addAttribute("stockToDisplay", stockToDisplay);
		
		mv.addObject("stockinfo", stockToDisplay);
		
		return mv;
	}
	public StockToDisplay getStock(String name) {
		StockToDisplay stock = new StockToDisplay();
		try {
			
            URL url = new URL("https://financialmodelingprep.com/api/v3/company/profile/AAPL");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            String out;
            String output = "";
            while ((out = br.readLine()) != null) {
                output += (out);
            }

            Gson gson= new Gson();
            stock = gson.fromJson(output, StockToDisplay.class);

            System.out.println(stock);
            return stock;
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }	
		return stock;
	}
	
}
