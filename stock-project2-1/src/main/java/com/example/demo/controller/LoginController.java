package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.Client;
import com.example.demo.repo.ClientRepo;

@Controller
@RequestMapping("login")
public class LoginController {
	@Autowired
	private ClientRepo clientRepo;
	
	@GetMapping
	public String displayLoginPage() {
		return "login/index";
	}
	
	@GetMapping("newlogin")
	public String displayNewLoginForm(Model model) {	
		model.addAttribute(new Client());
		return "login/newlogin";
	}
	@PostMapping("newlogin")
	public ModelAndView processNewUserForm(@ModelAttribute Client newClient, Model model) {		
		clientRepo.save(newClient);
		ModelAndView mv = new ModelAndView();
		mv.addObject("client", newClient);
		mv.setViewName("portfolio/index");
		return mv;
	}
	
	@GetMapping("existinglogin")
	public String displayExistLoginForm(Model model) {	
		model.addAttribute(new Client());
		return "login/existinglogin";
	}
	@PostMapping("existinglogin")
	public ModelAndView processExistUserForm(@ModelAttribute Client existClient, Model model) {		
		ModelAndView mv;
		if(loginVerification(existClient)) {
			mv = new ModelAndView();
			mv.setViewName("portfolio/index");
			mv.addObject("client", existClient);
			return mv;
		}
		mv = new ModelAndView("login/existinglogin");
		return mv;
		
	}
	
	//check username and password
	public boolean loginVerification(Client existclient) {
		List<Client> clients = clientRepo.findByUsername(existclient.getUsername());
		
		for(Client c: clients) {
			if(c.getPassword().equals(existclient.getPassword())){
				return true;
			}
		}	
		return false;
	}
}
