package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.Stock;
import com.example.demo.model.StockToDisplay;
import com.example.demo.repo.ClientRepo;
import com.example.demo.repo.StockRepo;

import DAO.StockDetail;

@Controller
@RequestMapping("stock")
public class StockController {
	@Autowired
	ClientRepo clientRepo;
	@Autowired
	StockRepo stockRepo;
	
	//stock/index
	@GetMapping
	public ModelAndView displayAllStock(Model model) {
		ModelAndView mv;
		mv = new ModelAndView();
		mv.setViewName("stock/index");		
		return mv;
	}
	
	@GetMapping("index")
	public String getAllStocks(Model model) {
		model.addAttribute(new StockToDisplay());
		return "stock/index";
	}
	
	//stock/searchstock
	@GetMapping("searchstock")
	public String searchStock(Model model) {
		model.addAttribute(new Stock());
		return "stock/searchstock";
	}
	
	@PostMapping("searchstock")
	public ModelAndView processStockSearchForm(@ModelAttribute Stock stock,Model model) {		
		ModelAndView mv;
		mv = new ModelAndView();
		mv.setViewName("stock/stockdetail");
		mv.addObject("stock", stock);
		StockToDisplay stockToDisplay = StockDetail.getStock(stock.getSymbol());
		mv.addObject("stockToDisplay", stockToDisplay);
		return mv;		
	}
		
	@GetMapping("stockdetail")
	public String stockDetail(Model model) {
		model.addAttribute(new StockToDisplay());
		return "stock/stockdetail";
	}
		
	@PostMapping("stockdetail")
	public String showStock(@ModelAttribute StockToDisplay newStock, Model model) {
		return "redirect:index";
	}
	
}

