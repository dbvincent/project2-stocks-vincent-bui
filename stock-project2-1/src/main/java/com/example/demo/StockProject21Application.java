package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockProject21Application {

	public static void main(String[] args) {
		SpringApplication.run(StockProject21Application.class, args);
	}

}
