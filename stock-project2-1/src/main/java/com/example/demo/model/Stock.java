package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stock")
public class Stock {
	
	@Id
	@GeneratedValue
	private int sid;
	private String symbol;
	private String companyName;
	private long shares;
	private double price;
	private String image;
	private String website;
	private int clientId;
	
	
	public Stock() {}
	public Stock(String symbol, String companyName, long shares, double price, String image, String website,
			int clientId) {
		super();
		this.symbol = symbol;
		this.companyName = companyName;
		this.shares = shares;
		this.price = price;
		this.image = image;
		this.website = website;
		this.clientId = clientId;
	}
	public int getSid() {
		return sid;
	}
	public void setSid(int sid) {
		this.sid = sid;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public long getShares() {
		return shares;
	}
	public void setShares(long shares) {
		this.shares = shares;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	@Override
	public String toString() {
		return "Stock [sid=" + sid + ", symbol=" + symbol + ", companyName=" + companyName + ", shares=" + shares
				+ ", price=" + price + ", image=" + image + ", website=" + website + ", clientId=" + clientId + "]";
	}
	
}
