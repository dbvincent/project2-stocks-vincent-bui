package com.example.demo.model;

public class Profile {
	
	private double price;
	private double beta;
	private long volAvg;
	private long mktCap;
	private double lastDiv;
	private String range;
	private String changes;
	private String changesPercentage;
	private String companyName;
	private String exchange;
	private String industry;
	private String website;
	private String description;
	private String ceo;
	private String sector;
	private String image;
		

	public Profile(double price, double beta, long volAvg, long mktCap, double lastDiv, String range,
			String changes, String changesPercentage, String companyName, String exchange, String industry,
			String website, String description, String ceo, String sector, String image) {
		this.price = price;
		this.beta = beta;
		this.volAvg = volAvg;
		this.mktCap = mktCap;
		this.lastDiv = lastDiv;
		this.range = range;
		this.changes = changes;
		this.changesPercentage = changesPercentage;
		this.companyName = companyName;
		this.exchange = exchange;
		this.industry = industry;
		this.website = website;
		this.description = description;
		this.ceo = ceo;
		this.sector = sector;
		this.image = image;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getBeta() {
		return beta;
	}
	public void setBeta(double beta) {
		this.beta = beta;
	}
	public long getVolAvg() {
		return volAvg;
	}
	public void setVolAvg(long volAvg) {
		this.volAvg = volAvg;
	}
	public long getMktCap() {
		return mktCap;
	}
	public void setMktCap(long mktCap) {
		this.mktCap = mktCap;
	}
	public double getLastDiv() {
		return lastDiv;
	}
	public void setLastDiv(double lastDiv) {
		this.lastDiv = lastDiv;
	}
	public String getRange() {
		return range;
	}
	public void setRange(String range) {
		this.range = range;
	}
	public String getChanges() {
		return changes;
	}
	public void setChanges(String changes) {
		this.changes = changes;
	}
	public String getChangesPercentage() {
		return changesPercentage;
	}
	public void setChangesPercentage(String changesPercentage) {
		this.changesPercentage = changesPercentage;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getExchange() {
		return exchange;
	}
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCeo() {
		return ceo;
	}
	public void setCeo(String ceo) {
		this.ceo = ceo;
	}
	public String getSector() {
		return sector;
	}
	public void setSector(String sector) {
		this.sector = sector;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	@Override
	public String toString() {
		return "Profile [price=" + price + ", beta=" + beta + ", volAvg=" + volAvg + ", mktCap=" + mktCap + ", lastDiv="
				+ lastDiv + ", range=" + range + ", changes=" + changes + ", changesPercentage=" + changesPercentage
				+ ", companyName=" + companyName + ", exchange=" + exchange + ", industry=" + industry + ", website="
				+ website + ", description=" + description + ", ceo=" + ceo + ", sector=" + sector + ", image=" + image
				+ "]";
	}
	
}

