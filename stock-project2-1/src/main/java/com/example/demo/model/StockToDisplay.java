package com.example.demo.model;

public class StockToDisplay {
	
	private String symbol;
	private Profile profile;
	
	public StockToDisplay(){}
	public StockToDisplay(String symbol, Profile profile) {
		this.symbol = symbol;
		this.profile = profile;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public Profile getProfile() {
		return profile;
	}
	public void setProfile(Profile profile) {
		this.profile = profile;
	}
	@Override
	public String toString() {
		return "Stock [symbol=" + symbol + ", profile=" + profile + "]";
	}
	
}
