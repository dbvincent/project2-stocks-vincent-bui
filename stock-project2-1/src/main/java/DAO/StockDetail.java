package DAO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.example.demo.model.StockToDisplay;
import com.google.gson.Gson;

public class StockDetail {
	public static StockToDisplay getStock(String symbol) {
		StockToDisplay stock = new StockToDisplay();
		try {
			
            URL url = new URL("https://financialmodelingprep.com/api/v3/company/profile/"+symbol);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            String out;
            String output = "";
            while ((out = br.readLine()) != null) {
                output += (out);
            }

            Gson gson= new Gson();
            stock = gson.fromJson(output, StockToDisplay.class);

            System.out.println("hello from inside getStock()"+stock);
            return stock;
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }	
		return stock;
	}	
}
